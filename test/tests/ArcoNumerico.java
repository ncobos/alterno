package tests;

import model.data_structures.digraph.IArco;

/**
 * Representa un arco sencillo con peso n�merico.
 */
public class ArcoNumerico implements IArco
{
    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------
	
    /**
	 * Constante para la serializaci�n 
	 */
	private static final long serialVersionUID = 1L;
	
    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
	
	/**
     * Peso del arco.
     */
    private int peso;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------
    
    /**
     * Constructor por par�metros.
     * @param peso Peso del arco.
     */
    public ArcoNumerico( int peso )
    {
        this.peso = peso;
    }
    
    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /* (non-Javadoc)
     * @see uniandes.cupi2.collections.grafo.IArco#darPeso()
     */
    public int darPeso( )
    {
        return peso;
    }

}
