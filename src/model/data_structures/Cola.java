package model.data_structures;

import java.util.Iterator;

public class Cola<T>implements Iterable<T>
{

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator();
	}

	public class ListIterator implements Iterator<T>
	{
		private Node current = first;
		public boolean hasNext()
		{ return current != null; }
		public void remove() { }
		public T next()
		{
			T item = current.item;
			current = current.next;
			return item;
		}
	}

	private Node first; // link to least recently added node
	private Node last; // link to most recently added node
	private int size; // number of items on the queue


	private class Node
	{ // nested class to define nodes
		T item;
		Node next;

		public Node next()
		{
			return next;
		}

		public T item()
		{
			return item;
		}
	}
	public boolean isEmpty() { return first == null; } // Or: N == 0.

	public int size() { return size; }

	
	public T dequeue()
	{ // Remove item from the beginning of the list.
		T item = first.item;
		first = first.next;
		if (isEmpty()) last = null;
		size--;
		return item;
	}

	public void enqueue(T ele) {
		// Add item to the end of the list.
			Node oldlast = last;
			last = new Node();
			last.item = (T) ele;
			last.next = null;
			if (isEmpty()) first = last;
			else oldlast.next = last;
			size++;
		
	}

	

}
