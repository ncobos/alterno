package model.data_structures;

import java.util.Iterator;

public class SeparateChainingHashTable<K extends Comparable<K>, V> 
{
	private int size;
	private int m;
	private DLLST<K, V>[] tabla;

	public SeparateChainingHashTable( int pTamano ) 
	{
		this.m = pTamano;
		tabla = (DLLST<K, V>[]) new DLLST[m];
		for (int i = 0; i < tabla.length; i++) 
		{
			tabla [i] = new DLLST<K, V>();
		}
	}

	public SeparateChainingHashTable(  ) 
	{
		this.m = 97;
		tabla = (DLLST<K, V>[]) new DLLST[m];
		for (int i = 0; i < tabla.length; i++) 
		{
			tabla [i] = new DLLST<K, V>();
		}
	}

	public int size() 
	{return size;}

	public void cambiarTamano(int nuevoTamano) throws Exception 
	{
		SeparateChainingHashTable<K, V> nuevo = new SeparateChainingHashTable<K, V>(nuevoTamano);
		for (int i = 0; i < tabla.length; i++) 
		{
			DLLST<K, V> lista = tabla[i];
			if(lista.getSize()!= 0)
			{

				while (lista.hasNext()) 
				{
					HashNode<K, V> elemento = lista.next();
					nuevo.put(elemento.darLlave(), elemento.getValue());
				}
			}
		}

		this.size = nuevo.size;
		this.tabla = nuevo.tabla;
		this.m = nuevo.m;
	}

	public int darPrimoSiguiente(int i) 
	{
		int r=i;

		while(true) 
		{
			r++;
			if(esPrimo(r)) 
			{
				break;
			}
		}

		return r;
	}
	public boolean esPrimo(int n) 
	{
		for(int i=2;i<n;i++) 
		{
			if(n%i==0)
				return false;
		}
		return true;
	}

	public void put(K llave, V valor) throws Exception
	{

		if(valor == null) 
		{
			delete(llave);
		}
		else if(contiene(llave)) 
		{
			int indice = hash(llave);

			tabla[indice].putElement(llave, valor);
		}
		else
		{
			if((size / m) >= 6) 
			{
				cambiarTamano(darPrimoSiguiente(m));
			}
			int indice = hash(llave);
			tabla[indice].add(valor, llave);
			size++;
		}
	}

	public boolean contiene(K llave) throws Exception 
	{
		return tabla[hash(llave)].existElement(llave);
	}

	public V get (K llave) throws Exception 
	{
		if(llave == null)throw new Exception();
		int hashCode= hash(llave);
		return tabla[hashCode].findElement(llave);
	}



	public void delete(K llave) throws Exception 
	{	
		if(contiene(llave)) 
		{
			int posicion = hash(llave);
			tabla[posicion].delete(llave);
			size--;
		}
	}

	public int hash(K llave) throws Exception 
	{	if(llave == null)throw new Exception();
	return llave.hashCode() % m;
	}

	public int tama�o() 
	{
		return size;
	}

	public Iterator<V> iterador() {
		Cola<V> cola = new Cola<V>();
		for (int i = 0; i < tabla.length; i++) 
		{
			DLLST<K, V> lista = tabla[i];
			if(lista.getSize() !=0) 
			{
				while (lista.hasNext()) 
				{
					HashNode<K, V> elemento = lista.next();
					cola.enqueue(elemento.getValue());
				}
			}

		}
		return cola.iterator();
	} 
	public Iterator<K> iteradork() {
		Cola<K> cola = new Cola<K>();
		for (int i = 0; i < tabla.length; i++) 
		{
			DLLST<K, V> lista = tabla[i];
			if(lista.getSize() !=0) 
			{
				while (lista.hasNext()) 
				{
					HashNode<K, V> elemento = lista.next();
					cola.enqueue(elemento.darLlave());
				}
			}

		}
		return cola.iterator();
	} 
}

