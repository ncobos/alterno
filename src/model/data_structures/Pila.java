package model.data_structures;
import java.util.Iterator;

public class Pila <T extends Comparable <T>> implements Iterable<T> , Stack{


	public class ListIterator implements Iterator<T>
	{
		private Node current = first;
		public boolean hasNext()
		{ return current != null; }
		public void remove() { }
		public T next()
		{
			T item = current.item;
			current = current.next;
			return item;
		}
	}
	private Node first; // link to least recently added node
	private Node last; // link to most recently added node
	private int size; // number of items on the queue


	private class Node
	{ // nested class to define nodes
		T item;
		Node next;

		public Node next()
		{
			return next;
		}

		public T item()
		{
			return item;
		}
	}

	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator();
	}

	@Override
	public void push(Comparable item ) {
		// Add item to top of stack.
		Node oldfirst = first;
		first = new Node();
		first.item = (T) item ;
		first.next = oldfirst;
		size++;
	}

	@Override
	public T pop() {
		// Remove item from top of stack.
		T item = first.item;
		first = first.next;
		size--;
		return item;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return first == null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}




}
