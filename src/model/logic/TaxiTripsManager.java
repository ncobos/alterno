package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.sun.org.apache.bcel.internal.generic.NEW;

import api.ITaxiTripsManager;
import model.data_structures.Queue;
import model.data_structures.Queue2;
import model.data_structures.Stack;
import model.data_structures.digraph.GrafoDirigido;
import model.data_structures.BST;
import model.data_structures.Cola;
import model.data_structures.HeapSort;
import model.data_structures.Lista;
import model.data_structures.Lista2;
import model.data_structures.LinkedList;
import model.data_structures.MergeSort;
import model.data_structures.Pila;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoDuracion;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;
import model.vo.ZonaServicios;
import sun.util.logging.resources.logging;

public class TaxiTripsManager implements ITaxiTripsManager
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	Cola <Service> colaJson = new Cola <Service>();
	//   

	Pila <Service> pilaJson = new Pila <Service>();

	Lista2 <Service> listaJson = new Lista2 <Service>();

	Lista <String> listaCom = new Lista <String>();

	Lista <String> listaZonas = new Lista <String>();

	Lista<String> listaDistancias = new Lista<String>(); 

	Lista<String> listaId = new Lista<String>(); 

	GrafoDirigido grafo = new GrafoDirigido();

	Lista<RangoDuracion> listaRango = new Lista <RangoDuracion>();

	BST<String, Lista<String>> bst1A = new BST<>();
	BST<String, Lista<String>> bst1B = new BST<>();
	BST <Double, FechaServicios> bst3C = new BST<>();
	BST<String, TaxiConServicios> bst7 = new BST<>();

	Lista<Integer> listaArea = new Lista <Integer>();

	Lista <Integer> listaDistancia = new Lista <Integer>();
	int max = 61;

	//1C
	public boolean cargarSistema(String direccionJson) 
	{
		boolean rta = false;

		JsonParser parser = new JsonParser();
		int max = 61;

		if (direccionJson.equals(DIRECCION_LARGE_JSON))
		{
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-08-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-07-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-06-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-05-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-04-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-03-02-2017.json");

		}

		try {
			String taxiTripsDatos = "./data/taxi-trips-wrvz-psew-subset-small.json";

			if(direccionJson.equals(DIRECCION_LARGE_JSON))
			{
				direccionJson = "./data/taxi-trips-wrvz-psew-subset-02-02-2017.json";
			}

			System.out.println("Inside loadServices with " + direccionJson);
			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(direccionJson));


			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				if(obj == null )
				{
					System.out.println("El objeto es nulo en: " + i);
				}

				/* Mostrar un JsonObject (Servicio taxi) */
				//System.out.println("------------------------------------------------------------------------------------------------");

				/* Obtener la propiedad company de un servicio (String) */
				String company = "NaN";
				if ( obj.get("company") != null)
				{ company = obj.get("company").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad dropoff_centroid_longitude de un servicio (String) */
				String dropoff_centroid_longitude = "NaN";
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad dropoff_centroid_latitude de un servicio (String)*/
				String dropoff_centroid_latitude = "NaN";
				if ( obj.get("dropoff_centroid_latitude") != null )
				{ dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				double latInicio= 0;
				if ( obj.get("pickup_centroid_latitude") != null)
				{ latInicio = obj.get("pickup_centroid_latitude").getAsDouble(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				double lonInicio= 0;
				if ( obj.get("pickup_centroid_longitude") != null)
				{ lonInicio = obj.get("pickup_centroid_longitude").getAsDouble();}

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip ID de un servicio (String) */
				String tripId = "NaN";
				if ( obj.get("trip_id") != null)
				{ tripId = obj.get("trip_id").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip seconds de un servicio (int) */
				int tripSec = 0;
				if ( obj.get("trip_seconds") != null)
				{ tripSec = obj.get("trip_seconds").getAsInt(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip miles de un servicio (double) */
				double tripMil= 0;
				if ( obj.get("trip_miles") != null)
				{ tripMil = obj.get("trip_miles").getAsDouble(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip total de un servicio (double) */
				double tripTot= 0;
				if ( obj.get("trip_total") != null)
				{ tripTot = obj.get("trip_total").getAsDouble(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad taxi ID de un servicio (String) */
				String taxiId = "NaN";
				if ( obj.get("taxi_id") != null)
				{ taxiId = obj.get("taxi_id").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad dropoff community area de un servicio (int) */
				int comArea = 0;
				if ( obj.get("dropoff_community_area") != null)
				{ comArea = obj.get("dropoff_community_area").getAsInt(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad pickup_community_area de un servicio (String) */
				int pickup_community_area= 0;
				if ( obj.get("pickup_community_area") != null)
				{ pickup_community_area = obj.get("pickup_community_area").getAsInt(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip_end_timestamp de un servicio (String) */
				String trip_end_timestamp= "NaN";
				if ( obj.get("trip_end_timestamp") != null)
				{ trip_end_timestamp = obj.get("trip_end_timestamp").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad  trip_start_timestamp de un servicio (String) */
				String  trip_start_timestamp= "NaN";
				if ( obj.get("trip_start_timestamp") != null)
				{  trip_start_timestamp = obj.get("trip_start_timestamp").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				JsonObject dropoff_localization_obj = null; 

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad dropoff_centroid_location (JsonObject) de un servicio*/
				if ( obj.get("dropoff_centroid_location") != null )
				{ dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad coordinates (JsonArray) de la propiedad dropoff_centroid_location (JsonObject)*/
				JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener cada coordenada del JsonArray como Double */
				double longitude = dropoff_localization_arr.get(0).getAsDouble();
				double latitude = dropoff_localization_arr.get(1).getAsDouble();}
				//System.out.println( "[Lon: " + longitude +", Lat:" + latitude + " ] (Datos double)");

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				Service servicio = new Service(tripId, tripSec, tripMil, tripTot, comArea,pickup_community_area, taxiId, company, trip_start_timestamp, trip_end_timestamp,latInicio,lonInicio);

				listaJson.add(servicio);
			}

			System.out.println("En esta lista hay: " + listaJson.size());

			if(listaJson.size()>0)
			{rta = true;}
		}

		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block1
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

		return rta;
	}


	public double toRad(double value)
	{
		return value * Math.PI / 180;
	}
	public double getDistance ( double lat1, double lon1, double lat2, double lon2 )
	{
		final int R = 6371*1000;
		double latDistance = toRad(lat2-lat1);
		double lonDistance = toRad(lon2-lon1);
		double a =  Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2* Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double d =R*c;
		return d/1609.344;

	}

	public void crearGrafo (double distanciaReferencia)
	{
		for ( Service actual: listaJson)
		{
			Double lati = actual.getPickupLatitud();
			Double logi = actual.getPickupLongitud();
			String llaveActual = lati +"_"+logi;
			Lista vertices = grafo.darVertices();
			for(int i= 0 ; i < vertices.darLongitud();i++)
			{
				 //  vertices.   
			}
			
//			if(grafo.vertexSize()==0)
//			{   
//				Vertex vertice = new Vertex<>(llaveActual);
//				grafo.addVertex(llaveActual, vertice);
//			}
//			else
//			{
//				Iterator<String> iter = grafo.iteradork();
//
//				Lista <String> sirven = new Lista();
//				while (iter.hasNext())
//				{
//					String act = iter.next();
//					String [] l= act.split("_");
//					if(getDistance(lati, logi, Double.parseDouble(l[0]), Double.parseDouble(l[1])) <= distanciaReferencia)
//					{
//						sirven.add(act);
//					}
//				}
//				if (grafo.darKey(llave)==null)
//				{
//					Lista <Service> x = new Lista <>;
//					x.add(actual);
//					grafo.addVertex(llave, x);
//				}
//			}
//		}
		}
	}
}

