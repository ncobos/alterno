package model.vo;

import model.data_structures.Lista;

public class TaxiConServicios implements Comparable<TaxiConServicios>{

	private String taxiId;
	private String compania;
	private Lista<Service> servicios;
	private int numeroServicios;
	

	public TaxiConServicios(String taxiId, String compania){
		this.taxiId = taxiId;
		this.compania = compania;
		this.servicios = new Lista<Service>(); // inicializar la lista de servicios 
	}

	public String getTaxiId() {
		return taxiId;
	}

	public String getCompania() {
		return compania;
	}

	public Lista<Service> getServicios()
	{
		return servicios;
	}

	public int numeroServicios(){
		return servicios.size();
	}

	public void agregarServicio(Service servicio){
		servicios.add(servicio);
	}

	public double distancia()
	{
		double rta = 0;
		
		for(Service servicio:servicios)
		{rta+= servicio.getTripMiles();}
		
		return rta;
	}
	
	public double dinero()
	{
		double rta = 0;
		
		for(Service servicio:servicios)
		{rta+= servicio.getTripTotalCost();}
		
		return rta;
	}
	
	public double tiempo()
	{
		double rta = 0;
		
		for(Service servicio:servicios)
		{rta+= servicio.getTripSeconds();}
		
		return rta;
	}

	@Override
	public int compareTo(TaxiConServicios o) {
		return 0;
	}

	public void print(){
		System.out.println(Integer.toString(numeroServicios())+" servicios "+" Taxi: "+taxiId);
		for(Service s : servicios){
			System.out.println("\t"+s.getTripStart());
		}
		System.out.println("___________________________________");;
	}
}
