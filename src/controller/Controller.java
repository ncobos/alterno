package controller;

import api.ITaxiTripsManager;
import model.data_structures.Lista;
import model.logic.TaxiTripsManager;
import model.vo.Service;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	
	//Carga El sistema
	public static boolean cargarSistema(String direccionJson)
	{
		return manager.cargarSistema(direccionJson);
	}
	public static void crearGrafo(int distancia)
	{
		manager.crearGrafo(distancia);
	}
}
